class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.belongs_to :user
      t.belongs_to :event
      t.text :content
      t.timestamps null: true
    end
  end
end
