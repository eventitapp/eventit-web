class ChangeDeviceTypeToString < ActiveRecord::Migration
  def change
    change_column :devices, :device_type, :string
  end
end
