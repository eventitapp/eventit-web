class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.timestamp :date
      t.text :description
      t.string :title
      t.decimal :lat
      t.decimal :lon
    end
  end
end
