class ChangeDeviceAttributes < ActiveRecord::Migration
  def change
    remove_column :devices, :device_type, :integer
    change_column :devices, :device_id, :string
    add_column :devices, :device_type, :integer
  end
end
