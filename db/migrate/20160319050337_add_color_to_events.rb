class AddColorToEvents < ActiveRecord::Migration
  def change
    add_column :events, :color, :string, default: '#303f9f'
  end
end
