class CreateDevices < ActiveRecord::Migration
  def change
    create_table :devices do |t|
      t.belongs_to :user
      t.integer :device_type
      t.integer :device_id
      t.timestamps null: false
    end
  end
end
