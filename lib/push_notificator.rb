class PushNotificator
  def initialize users, event
    @event = event
    @users = users
    @android_devices = Device.where(:user_id => @users.pluck('id'), :device_type => 0)
    @ios_devices = Device.where(:user_id => @users.pluck('id'), :device_type => 1)
  end

  def perform push_type
    create_android_push push_type unless @android_devices.empty?
    create_ios_push push_type unless @ios_devices.empty?
  end

  def create_android_push push_type
    n = Rpush::Gcm::Notification.new
    n.app = Rpush::Gcm::App.find_by_name("android_app")
    n.registration_ids = @android_devices.pluck('device_id')
    n.data = {push_type: push_type, event_id: @event.id, title: @event.title }
    n.save!
  end

  def create_ios_push push_type
    @ios_devices.each do|device|
      n = Rpush::Apns::Notification.new
      n.app = Rpush::Apns::App.find_by_name("ios_app")
      n.device_token = device.device_id
      n.alert = "New event.it Notification"
      n.data = {push_type: push_type, event_id: @event.id, title: @event.title }
      n.save!
    end
  end
end