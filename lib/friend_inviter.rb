class FriendInviter
  def initialize emails, event = nil
    @users = User.where(:email => emails)
    @send_invitations = emails - @users.pluck('email')
    @event = event
  end

  def perform
    if @event
      PushNotificator.new(@users, @event).perform 0
      @users.each{|user| @event.invite(user)}
    end
    @send_invitations.each{|email| UserMailer.invite_email(email).deliver_now}
  end
end