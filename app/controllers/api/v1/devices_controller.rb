module Api
  module V1
    class DevicesController < ApiController
      before_action :authenticate_user

      def gcm
        device = Device.where(device_id: params[:registration_id], device_type: 0).first
        if !device
          current_user.devices << Device.create(device_type: 0, device_id: params[:registration_id])
        else
          device.user = current_user
          device.save!
        end
        render :json => {status: 201}
      end

      def apns
        device = Device.where(device_id: params[:registration_id], device_type: 1).first
        if !device
          current_user.devices << Device.create(device_type: 1, device_id: params[:registration_id])
        else
          device.user = current_user
          device.save!
        end
        render :json => {status: 201}
      end

      def destroy_gcm
        device = current_user.devices.where(device_id: params[:id], device_type: 0).first
        device.destroy
        render :json => {status: 204}
      end

      def destroy_apns
        device = current_user.devices.where(device_id: params[:id], device_type: 1).first
        device.destroy
        render :json => {status: 204}
      end
    end
  end
end
