module Api
  module V1
    class ApiController < ApplicationController
      protect_from_forgery with: :null_session
      skip_before_filter :verify_authenticity_token
      respond_to :json


      protected

      def current_user
        User.find_by(:authentication_token => request.headers["X-User-Token"])
      end

      def authenticate_user
        render :json => {:status => 401, :messsage => "You must be logged in"} unless current_user
      end
    end
  end
end