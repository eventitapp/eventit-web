module Api
  module V1
    class EventsController < ApiController
      before_action :authenticate_user
      def index
        render :json => current_user.events.to_json(:include => {:users => {:only => [:id, :username]}})
      end

      def create
        event = Event.create(prepare_params)
        current_user.events << event
        if params[:users].any?
          FriendInviter.new(params[:users], event).perform
        end
        render :json => event.to_json(:include => {:users => {:only => [:id, :username]}})
      end

      protected

      def prepare_params
        {
          date: params[:date],
          title: params[:title],
          description: params[:description],
          color: params[:color]
        }
      end
    end
  end
end