module Api
  module V1
    class FriendsController < ApiController
      before_action :authenticate_user
      def send_invites
        FriendInviter.new(params[:emails]).perform
        render json: {status: 201}
      end
    end
  end
end
