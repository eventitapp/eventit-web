module Api
  module V1
    class MessagesController < ApiController
      before_action :authenticate_user

      def index
        @event = current_user.events.where(id: params[:event_id]).first
        if @event
        render json: formatted_messages.to_json
        else
          render json: {status: 401}
        end
      end

      def create
        @event = current_user.events.where(id: params[:event_id]).first
        if @event
          @event.messages.create(user_id: current_user.id, content: params[:message])
          notify = @event.users.where.not(id: current_user.id)
          PushNotificator.new(notify, @event).perform 1
          render json: {status: 201}
        else
          render json: {status: 401}
        end
      end

      protected
      def formatted_messages
        @event.messages.order(:created_at).map do |message|
          {
            id: message.id,
            username: message.user.username,
            me: current_user != message.user,
            content: message.content,
            date: message.created_at
          }
        end
      end
    end
  end
end
