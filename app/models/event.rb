class Event < ActiveRecord::Base

  has_many :users, through: :participations
  has_many :participations
  has_many :messages

  def invite user
    self.users << user
  end

end
