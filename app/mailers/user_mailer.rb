class UserMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def invite_email(email)
    @url  = 'http://example.com/login'
    mail(to: email, subject: 'Invite to event.it')
  end
end
